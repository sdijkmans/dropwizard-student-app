package dropwizard.api;

import com.fasterxml.jackson.annotation.JsonProperty;

public class StudentDTO {
    private long id;
    private String name;
    private int age;

    public StudentDTO() {
        // Jackson deserialization
    }

    public StudentDTO(long id, String name, int age) {
        this.id = id;
        this.name = name;
        this.age = age;
    }

    @JsonProperty
    public long getId() {
        return id;
    }

    @JsonProperty
    public String getName() {
        return name;
    }
    
    @JsonProperty
    public int getAge() {
        return age;
    }

}