package dropwizard.health;

import com.codahale.metrics.health.HealthCheck;

import dropwizard.db.StudentDAO;

public class DatabaseHealthCheck extends HealthCheck {
    private final StudentDAO database;

    public DatabaseHealthCheck(StudentDAO dao) {
        this.database = dao;
    }

    @Override
    protected Result check() throws Exception {    	
    	try {
    		database.findNameById(0);
    		return Result.healthy();
    	} catch (Exception e) {
    		return Result.unhealthy("couldn't fetch data from database");
    	}
    }
}