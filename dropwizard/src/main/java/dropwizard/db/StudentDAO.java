package dropwizard.db;

import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.SqlQuery;

public interface StudentDAO {
	
	@SqlQuery("select name from student where id= :id")
	String findNameById(@Bind("id") int id);

}
