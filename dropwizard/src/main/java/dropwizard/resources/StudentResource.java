package dropwizard.resources;


import dropwizard.api.StudentDTO;
import dropwizard.db.StudentDAO;
import com.codahale.metrics.annotation.Timed;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import java.util.Optional;

@Path("/student")
@Produces(MediaType.APPLICATION_JSON)
public class StudentResource {
	
	private StudentDAO dao;
	
	public StudentResource(StudentDAO dao) {
		this.dao = dao;
	}
	
    @GET
    @Timed
    @Path("/random")
    public StudentDTO getRandomStudent() {
    	return new StudentDTO(1,"Seppe",25);
    }

    @GET
    @Timed
    public String getNameOfStudentWithId(@QueryParam("id") Optional<Integer> id) {
        return dao.findNameById(id.get());
    }
    

}