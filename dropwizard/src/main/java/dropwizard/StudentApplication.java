package dropwizard;

import org.skife.jdbi.v2.DBI;

import dropwizard.db.StudentDAO;
import dropwizard.health.DatabaseHealthCheck;
import dropwizard.resources.StudentResource;
import io.dropwizard.Application;
import io.dropwizard.jdbi.DBIFactory;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;

public class StudentApplication extends Application<StudentConfiguration> {

	public static void main(final String[] args) throws Exception {
		new StudentApplication().run(args);
	}

	@Override
	public String getName() {
		return "Student Rest Service";
	}

	@Override
	public void initialize(final Bootstrap<StudentConfiguration> bootstrap) {
		// TODO: application initialization
	}

	@Override
	public void run(final StudentConfiguration configuration, final Environment environment) {
		final StudentDAO dao = fetchStudentDAOWithDB(configuration, environment);
	    environment.jersey().register(new StudentResource(dao));
	    environment.healthChecks().register("DatabaseHealthCheck", new DatabaseHealthCheck(dao));
	}

	private StudentDAO fetchStudentDAOWithDB(StudentConfiguration configuration, Environment environment) {
	    final DBIFactory factory = new DBIFactory();
	    final DBI jdbi = factory.build(environment, configuration.getDatabaseConfiguration(), "mysql");
	    return jdbi.onDemand(StudentDAO.class);
	}

}
